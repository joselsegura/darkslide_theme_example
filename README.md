# Dependencies

pip install darkslide

# Generation

- Using `default` theme:

`darkslide slides.md`

- Using custom theme:

`darkslide -t mytheme slides.md`

# Viewing the result

`firefox presentation.html`
