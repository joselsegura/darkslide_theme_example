# Install Party #1

.footer: qbitacademy - 2020

---

# index()

.notes: notes

* Hello world, qbitacademy!
* Aclarando conceptos
* Instalación
* Resolución de problemas
* Preguntas


---

# Hello world, qbitacademy!

---

# ¿Quién somos?

* Academia de programación y robótica
    * Little Hackers (6 a 12 años)
    * Junior Hackers (12 a 18 años)
    * Universidad
    * Profesionales

* Enseñar las virtudes de la programación
* Preparar a la sociedad para los retos de hoy

---

# Aclarando conceptos

> GNU, Linux, GNU/Linux, Debian, ...

---

# Lists

## Unordered List

- Markdown
- ReStructured Text
- Textile

## Ordered List

1. Python
2. JavaScript
3. HTML5

---

# Second Title Slide

---

# Code

## Single Word

Hello `World`

## Python

    !python
    def multiply (x, y):
        return x * y

## JavaScript

    !javascript
    multiply: function (x, y) {
      return x * y;
    }

## HTML

    !html
    <!doctype html>
    <html>
      <head></head>
      <body></body>
    </html>

---

# Images

![Landscape](../_assets/landscape.jpg)

---

# View Presenter Notes

This slide has presenter notes. Press `p` to view them.

# Presenter Notes

Hello from presenter notes

---

# TOC and subsections

A presentation supports multiple section levels. Subsections can be used to organize contents.

The global TOC includes sections and subsections.

The max-toc-level parameter allows the user to limit the number of subsections included in the global TOC.

---

## Subsection A

This slide is a subsection of the section "Toc and subsections"

---

## Subsection B

This slide is a subsection of the section "Toc and subsections"

---

# Other features

View other features in the help sidebar by pressing `h`
